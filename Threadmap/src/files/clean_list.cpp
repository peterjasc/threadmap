//&>/dev/null;x="${0%.*}";[ ! "$x" -ot "$0" ]||(rm -f "$x";g++ -std=c++11 -o "$x" "$0")&&exec "$x" "$@"

#include <iostream>
#include <algorithm>
#include <cctype>
#include <set>

using namespace std;

/**
 * Reads words seperated by whitespace,
 * removes duplicates and prints them in sorted order
 */
int main()
{
  string word;
  set<string> words;

  while (cin >> word)
  {
    transform(word.begin(), word.end(), word.begin(), ::tolower);
    words.insert(word);
  }

  for (string const& w : words)
    cout << w << '\n';

  return 0;
}

// ./newline.cpp < Conjunctions.txt > C.txt
// g++ newline.cpp -o newline -std=c++11 && ./newline < Conjunctions.txt > C.txt
