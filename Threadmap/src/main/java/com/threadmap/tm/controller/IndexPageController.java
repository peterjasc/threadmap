package com.threadmap.tm.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.threadmap.SearchLib.database.*;
import com.threadmap.SearchLib.wordcount.WordCount;

@Controller
public class IndexPageController {

	@RequestMapping("/")
	public String home(Model map) {
		List<String> words = new ArrayList<String>();
		List<Integer> counts = new ArrayList<Integer>();
		
		
		try {
			ISearchResults sRes = DatabaseFactory.createSearcher(30);
			List<WordCount> wordCount = sRes.findMostPopular();
			
			for (WordCount w : wordCount) {
				
				words.add(w.word);
				counts.add(w.count);
				
				System.out.println(w.count);
				System.out.println(w.word);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		map.addAttribute("words", words);
		map.addAttribute("counts", counts);
		return "index";
	}

}
