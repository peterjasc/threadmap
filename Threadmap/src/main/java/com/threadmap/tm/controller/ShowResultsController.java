package com.threadmap.tm.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.threadmap.SearchLib.database.ISearchResults;
import com.threadmap.SearchLib.database.Results;
import com.threadmap.SearchLib.database.DatabaseFactory;
import com.threadmap.SearchLib.wordcount.WordCount;

@Controller
public class ShowResultsController {

	String message = "Results for";
	private static final int MAX_RESULT_SIZE = 100;

	@RequestMapping("/showResults")
	public ModelAndView showMessage(

			@RequestParam(value = "word", required = false, defaultValue = "World") String word)
			throws IOException {
		
		System.out.println("I am in Show Results controller");
		ModelAndView mv = new ModelAndView("results");
		
		Results results = null;
		try (ISearchResults db = DatabaseFactory.createSearcher(MAX_RESULT_SIZE)) {
			results = db.search(word);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if (results != null && !results.urls.isEmpty()) {
			mv.addObject("word", word);
			mv.addObject("urls", results.urls);
			mv.addObject("counts", results.counts);
			mv.addObject("shares", results.shares);
			mv.addObject("message", "Here are the results for your query:");
			
		} else {
			mv.addObject("message", "No such word found in database!");
		}
		
		List<String> words = new ArrayList<String>();
		List<Integer> counts = new ArrayList<Integer>();
		
		try {
			ISearchResults sRes = DatabaseFactory.createSearcher(30);
			List<WordCount> wordCount = sRes.findConnected(word);
			
			for (WordCount w : wordCount) {
				
				words.add(w.word);
				counts.add(w.count);
				
				System.out.println(w.count);
				System.out.println(w.word);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		mv.addObject("popWords", words);
		mv.addObject("popCounts", counts);
		return mv;

	}

}
