<script src="http://code.jquery.com/jquery-latest.min.js"
	type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="resources/css/main.css">
<script src="resources/js/main.js"></script>

<script src="resources/js/jquery.tagcloud.js"></script>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<link
	href='http://fonts.googleapis.com/css?family=Patrick+Hand&subset=vietnamese,latin-ext,latin'
	rel='stylesheet' type='text/css'>

<header class="navbar-fixed-top">
	<div class="hButtons">
		<a href="/Threadmap">Threadmap </a>
	</div>
</header>