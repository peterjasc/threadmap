<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<jsp:include page="/WEB-INF/views/imports.jsp" />
<title>Threadmap Index Page</title>
</head>

<body>
	<div class="jumbotron-index">
		<div class="container">
			<br>
			<div class="moto">Better information leads to better decisions</div>
			<div class="form">
				<div class="row">
					<form class="input-group">
						<input id="insert-link-form" type="text" class="form-control">
						<span class="input-group-btn"> <input
							class="btn btn-default" type="submit" value="GET RESULTS">
						</span>
					</form>
					<!-- /input-group -->
					<div class="not-valid-error"></div>
				</div>
				<!-- /.row -->
			</div>
		</div>
		<div class="tagCloud">
			<div id="tagCloud">
				<c:forEach items="${words}" var="element" varStatus="status">
					<a href="/Threadmap/showResults?word=${element}"
						rel="${counts.get(status.index)}">${element}</a>
				</c:forEach>
			</div>
		</div>
		<br>

	</div>
</body>
</html>