<%@ page language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:include page="imports.jsp" />
<title>Results</title>
</head>
<body>

	<div class="jumbotron col-md-12">
		<div class="resultMsg col-md-6">${message}<br>
			<div class="form">
				<div class="results">
					<form class="input-group">
						<input id="insert-link-form" type="text" value="${word}" class="form-control">
						<span class="input-group-btn"> <input
							class="btn btn-default" type="submit" value="GET RESULTS">
						</span>
					</form>
					<!-- /input-group -->
					<div class="not-valid-error"></div>
				</div>
				<!-- /.row -->
			</div>
		</div>
		<div class="tagCloudResults col-md-6">
			<div id="tagCloud">
				<c:forEach items="${popWords}" var="element" varStatus="status">
					<a href="/Threadmap/showResults?word=${word}%20${element}"
						rel="${popCounts.get(status.index)}">${element}</a>

				</c:forEach>
			</div>
		</div>
	</div>

	<div id="resultsTable">
		<table class="table table-hover">
			<br>
			<tr>
				<th>URL</th>
				<th>SHARE COUNT</th>
			</tr>
			<c:forEach items="${urls}" var="element" varStatus="status">
				<tr>
					<td><a href="${element}">${element}</a></td>
					<td>${shares.get(status.index)}</td>
				</tr>
			</c:forEach>
		</table>
	</div>

</body>
</html>