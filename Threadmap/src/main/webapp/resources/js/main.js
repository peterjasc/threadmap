$(document).ready(function(){
    $('form').on('submit', function(e){
        e.preventDefault();
        var val = $("#insert-link-form").val();
        if (val != "") {
        	window.location.href = "showResults?word=" + val;
        } else {
        	//$( ".not-valid-error" ).text( "Not valid!" ).show().fadeOut( 1000 );
        }
    });
    
    $.fn.tagcloud.defaults = {
    		  size: {start: 16, end: 28, unit: 'px'},
    		  color: {start: '#a8a8a8', end: '#ffffff'}
    		};
    
    $('#tagCloud a').tagcloud();
    
    
});

